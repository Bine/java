package unit;

import abilities.DefaultAttack;
import exeption.FielBusyException;
import exeption.InvalidParamException;
import exeption.UnitIsDeadException;

import static unit.Classes.*;

public class Berserker extends Unit {
	
	public Berserker(String name, int hp, int damage) throws FielBusyException, InvalidParamException {
		super(name, hp, 3);
		this.ability = new DefaultAttack(this, damage);
		unit_class = BERSERKER;
		this.image = "Berserk";
	}
	
	public void takeMagicDamage(int damage) throws UnitIsDeadException {}

}
