package unit;

import static unit.Classes.*;

import abilities.RogueAttack;
import exeption.FielBusyException;
import exeption.InvalidParamException;

public class Rogue extends Unit {
	
	public Rogue(String name, int hp, int damage) throws FielBusyException, InvalidParamException {
		super(name, hp, 4);
		this.ability = new RogueAttack(damage);
		unit_class = ROGUE;
		this.image = "Rogue";
	}

}
