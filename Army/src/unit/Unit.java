package unit;

import java.awt.Image;
import java.util.HashSet;
import java.util.Set;

import javax.swing.ImageIcon;


import abilities.Ability;
import abilities.WerewolfAttack;
import exeption.FielBusyException;
import exeption.InvalidParamException;
import exeption.UnitIsDeadException;
import main.Main;
import state.State;


public abstract class Unit {
	protected int step;
	protected Classes unit_class;
	protected String name;
	protected State state;
	protected Ability ability;
	protected HashSet<Unit> observers = new HashSet<Unit>();
	protected Ability vampireAbility;
	protected State altState;
	protected Ability altAbility;
	protected boolean switch_form = false;
	protected String image;

	public Unit(String name, int hp,  int step) throws FielBusyException, InvalidParamException {
		this(name, hp, 0,  step);
	}
	
	public Unit(String name, int hp, int mana,  int step) throws FielBusyException, InvalidParamException {
		state = new State(hp, mana);
		this.name = name;
		if ( step < 1 ) {
			throw new InvalidParamException();
		} else {
			this.step = step;
		}
		
//		if ( Board.board[location.getX()][location.getY()] == 1 ) {
//			throw new FielBusyException();
//		} else {
//			Board.board[location.getX()][location.getY()] = 1;
//		}
	}
	
	public State getState() {
		return this.state;
	}
	public String getName() {
		return this.name;
	}
	public Ability getAbility() {
		return this.ability;
	}
	public Classes getUnit_class() {
		return this.unit_class;
	}
	public Ability getVampireAbility() {
		return vampireAbility;
	}
	public int getStep() {
		return this.step;
	}
	
	
	public void setUnit_class(Classes unit_class) {
		this.unit_class = unit_class;
	}
	public void setAbility(Ability otherAbility) {
		this.ability = otherAbility;
	}
	
	public void takeDamage(int damage) throws UnitIsDeadException {
		isAlive();
		this.state.hp -= damage;
		if (this.state.hp <= 0) {
			this.state.hp = 0;
		}
	}

	public void takeMagicDamage(int damage) throws UnitIsDeadException {
		takeDamage(damage);
	}

	public void takeHeal(int heal) throws UnitIsDeadException {
		isAlive();
		this.state.hp += heal;
		if (this.state.hp > this.state.maxhp)
			this.state.hp = this.state.maxhp;
	}

	public void addObserver(Unit unit) {
		observers.add(unit);
	}

	public void die() throws UnitIsDeadException {
		for (Unit u : this.observers) {
			u.takeHeal(30);
		}
	}

	public void isAlive() throws UnitIsDeadException {
		if (this.state.hp == 0) {
			die();
			throw new UnitIsDeadException();
		}
	}

	public void	attack(Unit enemy) throws Exception {
		isAlive();
		ability.action(enemy);
	}

	public void transform() {
		if ( state.isWerewolf ) {
			State tempState = state;
			Ability tempAbility = ability;
			
			if ( switch_form == false) {
			altAbility = new WerewolfAttack(this, ability.getDamage() * 2);
			switch_form = true;
			} else {
				altAbility = new WerewolfAttack(this, ability.getDamage() / 2);
				switch_form = false;
			}
			
			altState = new State(state.maxhp * 2);
			state = altState;
			ability = altAbility;
			altAbility = tempAbility;
			altState = tempState;
		
			state.hp = state.maxhp * altState.hp / altState.maxhp;
		}
	}

	public Image getUnitImage() {
		return  new ImageIcon(Main.class.getResource("/image/" + image + ".png")).getImage();
	}
	
	public void move(int x, int y) throws FielBusyException, InvalidParamException {
//		move(new Point(x, y));
	}
	
//	public void move(Point other) throws FielBusyException, InvalidParamException {
//		if ( Board.board[other.getX()][other.getY()] == 1 ) {
//			throw new FielBusyException();
//		}
//		int distance = this.location.distance(other);
//		if ( this.step >= distance ) {
//			Board.board[location.getX()][location.getY()] = 0;
//			this.location = other;
//			Board.board[location.getX()][location.getY()] = 1;
//		} else {
//			throw new InvalidParamException();
//		}
//		
//	}
	
	public String toString() {
		StringBuilder str = new StringBuilder();
		str.append(getName());
		str.append(": hp(");
		str.append(getState().hp);
		str.append("/");
		str.append(getState().maxhp);
		str.append(")");

		return str.toString();
	}
}

