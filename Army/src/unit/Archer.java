package unit;

import static unit.Classes.*;

import abilities.ArcherAttack;
import exeption.FielBusyException;
import exeption.InvalidParamException;

public class Archer extends Unit {
	
	public Archer(String name, int hp, int damage) throws FielBusyException, InvalidParamException {
		super(name, hp, 2);
		this.ability = new ArcherAttack(this, damage);
		unit_class = ARCHER;
		this.image = "Archer";
	}

}
