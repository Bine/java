package unit.spellcaster;

import static unit.Classes.*;

import exeption.FielBusyException;
import exeption.InvalidParamException;
import exeption.NotEnoughManaException;
import exeption.UnitIsDeadException;
import spells.Fireball;
import spells.Spells;
import unit.Soldier;

public final class Warlock extends Spellcaster {
	private Demon demon = null;

	public Warlock(String name, int hp, int mana) throws FielBusyException, InvalidParamException {
		super(name, hp, mana);
		isBattleMagician = false;
		spells.put(Spells.FIREBALL, new Fireball(this, 30, 30));
		unit_class = WARLOCK;
		this.image = "Warlock";
	}
	
	public Demon toCommandDemon() {
		return this.demon;
	}
	
	public void summon() throws NotEnoughManaException, FielBusyException, InvalidParamException {
		if( demon == null || demon.getState().hp <= 0 ) {
			spendMana(30);
			demon = new Demon("Demon", 150, 40);
		}
	}

}

final class Demon extends Soldier {
	private int defence;
	
	public Demon(String name, int hp, int damage) throws FielBusyException, InvalidParamException {
		super(name, hp, damage);
		unit_class = null;
	}
	
	public void takeDamage(int damage) throws UnitIsDeadException {
		isAlive();
		damage -= defence;
		this.state.hp -= damage;
		if (this.state.hp <= 0) {
			this.state.hp = 0;
		}
	}
}
