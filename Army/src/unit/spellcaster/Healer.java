package unit.spellcaster;

import static unit.Classes.WIZARD;

import exeption.FielBusyException;
import exeption.InvalidParamException;
import spells.Fireball;
import spells.Heal;
import spells.Spells;

public class Healer extends Spellcaster {

	public Healer(String name, int hp, int mana) throws FielBusyException, InvalidParamException {
		super(name, hp, mana);
		isBattleMagician = false;
		spells.put(Spells.FIREBALL, new Fireball(this, 30, 30));
		spells.put(Spells.HEAL, new Heal(this, 30, 30));
		unit_class = WIZARD;
		this.image = "Healer";
	}

}
