package unit.spellcaster;

import static unit.Classes.*;

import exeption.FielBusyException;
import exeption.InvalidParamException;
import exeption.TargetLongAwayException;
import spells.Fireball;
import spells.Spells;
import unit.Unit;

public class Necromancer extends Spellcaster {

	public Necromancer(String name, int hp, int mana) throws FielBusyException, InvalidParamException {
		super(name, hp, mana);
		isBattleMagician = false;
		spells.put(Spells.FIREBALL, new Fireball(this, 30, 30));
		unit_class = NECROMANCER;
		this.image = "Necromancer";
	}
	
	public void	attack(Unit enemy) throws Exception {
		isAlive();
//		int distance = this.location.distance(enemy.getLocation());
//		if ( ability.getRadiusAttack() == distance ) {
			enemy.addObserver(this);
			ability.action(enemy);
//		} else {
//			throw new TargetLongAwayException();
//		}
	}

}
