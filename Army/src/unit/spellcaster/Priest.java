package unit.spellcaster;

import static unit.Classes.*;

import abilities.PriestAttack;
import exeption.FielBusyException;
import exeption.InvalidParamException;
import spells.*;

public class Priest extends Spellcaster {
	

	public Priest(String name, int hp, int mana) throws FielBusyException, InvalidParamException { 
		super(name, hp, mana);
		ability = new PriestAttack(this, 16);
		isBattleMagician = false;
		spells.put(Spells.SACREDFIRE, new SacredFire(this, 30, 30));
		spells.put(Spells.HEAL, new Heal(this, 30, 30));
		unit_class = PRIEST;
		this.image = "Priest";
	}
}
