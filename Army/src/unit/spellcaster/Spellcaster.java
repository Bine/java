package unit.spellcaster;

import java.util.HashMap;
import abilities.DefaultAttack;
import exeption.FielBusyException;
import exeption.InvalidParamException;
import exeption.NotEnoughManaException;
import exeption.TargetLongAwayException;
import spells.Spell;
import spells.Spells;
import unit.Unit;

public abstract class Spellcaster extends Unit {
	public boolean isBattleMagician;
	protected HashMap<Spells, Spell> spells = new HashMap<Spells, Spell>();
    
    public Spellcaster(String name, int hp, int mana) throws FielBusyException, InvalidParamException {
    	super(name, hp, mana, 2);
    	ability = new DefaultAttack(this, 16);
    }
    
	public void spendMana(int amount) throws NotEnoughManaException {
		int newManaAmount = state.mana - amount;
		
		if ( newManaAmount < 0) {
			throw new NotEnoughManaException();
		}
		
		state.mana = newManaAmount;
	}
	
	public void cast(Spells spell, Unit target) throws Exception {
		isAlive();
		
		Spell currentSpell = spells.get(spell);
		
//		int distance = this.location.distance(target.getLocation());
//		if ( currentSpell.getRadiusCast() >= distance ) {
			spendMana(currentSpell.spellcost);
			currentSpell.action(target);
//		} else { 
//			throw new TargetLongAwayException();
//		}
	}
	
	public String toString() {
		StringBuilder str = new StringBuilder();
		str.append(getName());
		str.append(": hp(");
		str.append(getState().hp);
		str.append("/");
		str.append(getState().maxhp);
		str.append(") mp(");
		str.append(getState().mana);
		str.append("/");
		str.append(getState().maxmana);
		str.append(")");
		
		return str.toString();
	}
}
