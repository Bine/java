package unit;

import static unit.Classes.*;

import abilities.VampireAttack;
import exeption.FielBusyException;
import exeption.InvalidParamException;

public class Vampire extends Unit {
	
	public Vampire(String name, int hp, int damage) throws FielBusyException, InvalidParamException {
		super(name, hp, 3);
		this.ability = new VampireAttack(this, damage);
		unit_class = VAMPIRE;
		this.image = "Vampire";
	}

}
