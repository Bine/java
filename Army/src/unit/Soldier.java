package unit;

import static unit.Classes.*;

import abilities.DefaultAttack;
import exeption.FielBusyException;
import exeption.InvalidParamException;

public class Soldier extends Unit {
	
	public Soldier(String name, int hp, int damage) throws FielBusyException, InvalidParamException {
		super(name, hp, 3);
		this.ability = new DefaultAttack(this, damage);
		unit_class = SOLDIER;
		this.image = "Soldier";
	}

}
