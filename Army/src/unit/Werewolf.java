package unit;

import static unit.Classes.*;

import abilities.DefaultAttack;
import exeption.FielBusyException;
import exeption.InvalidParamException;

public class Werewolf extends Unit {

	public Werewolf(String name, int hp, int damage) throws FielBusyException, InvalidParamException {
		super(name, hp, 3);
		this.ability = new DefaultAttack(this, damage);
		state.isWerewolf = true;
		unit_class = WEREWOLF;
		this.image = "Werewolf";
	}

}
