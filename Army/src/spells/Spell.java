package spells;

import unit.Unit;
import unit.spellcaster.Spellcaster;

public abstract class Spell {
	protected Spellcaster owner;
	public int spellpower;
	public int spellcost;
	private int radiusCast;
	
	public Spell(Spellcaster owner, int spellpower, int spellcost, int radius) {
		this.spellpower = spellpower;
		this.spellcost = spellcost;
		this.owner = owner;
		this.radiusCast = radius;
	}
	
	public int getRadiusCast() {
		return this.radiusCast;
	}
	
	public abstract void action(Unit target);
}
