package spells;

import unit.Unit;
import unit.spellcaster.Spellcaster;
import static unit.Classes.*;

public class SacredFire extends Spell {

	public SacredFire(Spellcaster owner, int spellpower, int spellcost) {
		super(owner, spellpower, spellcost, 5);
		if ( !owner.isBattleMagician ) {
			this.spellpower /= 2;
		}
	}

	@Override
	public void action(Unit target) {
		int newHp;
		
		if ( target.getUnit_class() == VAMPIRE || target.getUnit_class() == NECROMANCER ) {
			newHp = target.getState().hp - ( spellpower * 2 );
		} else {
			newHp = target.getState().hp - spellpower;
		}
		
		if ( newHp < 0 ) {
			target.getState().hp = 0;
		} else {
			target.getState().hp = newHp;
		}
	}

}
