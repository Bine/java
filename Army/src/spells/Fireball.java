package spells;

import unit.Unit;
import unit.spellcaster.Spellcaster;

public class Fireball extends Spell {

	public Fireball(Spellcaster owner, int spellpower, int spellcost) {
		super(owner, spellpower, spellcost, 5);
		if ( !owner.isBattleMagician ) {
			this.spellpower /= 2;
		}
	}

	@Override
	public void action(Unit target) {
		int newHp = target.getState().hp - spellpower;
		
		if ( newHp < 0 ) {
			target.getState().hp = 0;
		} else {
			target.getState().hp = newHp;
		}
	}

}
