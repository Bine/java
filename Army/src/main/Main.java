package main;

import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Rectangle2D;

import javax.swing.*;

import board.Board;
import unit.spellcaster.Spellcaster;

public class Main {
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				SimpleFrame frame = new SimpleFrame();
				frame.setTitle("Heroes 1.0");
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame.setExtendedState(Frame.MAXIMIZED_BOTH);
				frame.setVisible(true);
			}
		});
	}


static class SimpleFrame extends JFrame {
	private int DEFAULT_WIDTH = 1024;
	private int DEFAULT_HEIGHT = 800;

	public SimpleFrame() {
		BorderLayout bl = new BorderLayout();
		setLayout(bl);

		JLabel lbl = new JLabel("Game Board");
		lbl.setHorizontalAlignment(SwingConstants.CENTER);
		add(lbl);

		bl.addLayoutComponent(lbl, BorderLayout.NORTH);

		DrawComponent dc = new DrawComponent();
		dc.addMouseListener(new MouseAdapter() {
		@Override
		public void mouseClicked(MouseEvent e) {		
			int x = (int) ((e.getX()-dc.x)/(dc.fieldWidth/8));
			int y = (int) ((e.getY()-dc.y)/(dc.fieldHeight/8));
			int distance;
			
			if (e.getButton() == MouseEvent.BUTTON1) {

				if ( Board.selectedUnit == null ) {
					if ( Board.board[x][y] == null ) {
						System.out.println("You have not selected unit");
					} else {
						Board.selectedUnit = new Point(x, y);
						dc.repaint();
					}
				} else  {
					distance = Board.distance(Board.selectedUnit, x, y);
					if ( x == Board.selectedUnit.getX() && y == Board.selectedUnit.getY() ) {
						Board.selectedUnit = null;
						dc.repaint();
					} else if( Board.board[x][y] == null ) {
						if ( Board.board[(int) Board.selectedUnit.getX()][(int) Board.selectedUnit.getY()].getStep() >= distance ) {
							Board.board[x][y] = Board.board[(int) Board.selectedUnit.getX()][(int) Board.selectedUnit.getY()]; 
							Board.board[(int) Board.selectedUnit.getX()][(int) Board.selectedUnit.getY()] = null;
							Board.selectedUnit = null;
							
							dc.repaint();
						} else {
							System.out.println("Step so small");
						}
					} else {
						if ( Board.board[(int) Board.selectedUnit.getX()][(int) Board.selectedUnit.getY()].getAbility().getRadiusAttack() >= distance ) {
							try {
								Board.board[(int) Board.selectedUnit.getX()][(int) Board.selectedUnit.getY()].attack(Board.board[x][y]);
								System.out.println(Board.board[x][y]);
								System.out.println("Attack!");
								if(Board.board[x][y].getState().hp == 0) {
									Board.board[x][y] = null;
								}
								dc.repaint();
							} catch (Exception e1) {
								e1.printStackTrace();
							}
						} else {
							System.out.println("Small radius attack");
						}
					}
					
				}
			}	else if ( e.getButton() == MouseEvent.BUTTON3 && Board.board[x][y] instanceof Spellcaster) {
				
			}		
			
//			dc.repaint();
//			System.out.println(Board.board[x][y].toString());	
		}
		});
		
		add(dc);
		bl.addLayoutComponent(dc, BorderLayout.CENTER);
		dc.setBackground(Color.BLACK);
		
		setSize(DEFAULT_WIDTH, DEFAULT_HEIGHT);
	}
}

	static class DrawComponent extends JComponent {

		private double x = 0.0;
		private double y = 0.0;
		private double fieldWidth = 0.0;
		private double fieldHeight = 0.0;

		public static final int DEFAULT_WIDTH = 400;
		public static final int DEFAULT_HEIGHT = 400;
		public static final Image image = new ImageIcon(Main.class.getResource("/image/grass_1.jpg")).getImage();

		public void paintComponent(Graphics g) {

			Graphics2D g2 = (Graphics2D) g;

			int width = getWidth();
			int height = getHeight();

			fieldWidth = height - (height / 4);
			fieldHeight = fieldWidth;

			x = (width - fieldWidth) / 2;
			y = (height - fieldHeight) / 2;

			double cellWidth = fieldWidth / 8;
			double cellHeight = cellWidth;

			g2.setColor(new Color(153, 153, 153));
			g2.fillRect(0, 0, width, height);

			g2.drawImage(image, (int) x, (int) y, (int) fieldWidth + 1, (int) fieldHeight + 1, this);

			g2.setColor(Color.black);
			g2.setStroke(new BasicStroke(2));

			for (int i = 0; i < 9; i++) {
				g2.drawLine((int) (x + cellWidth * i), (int) y, (int) (x + cellHeight * i), (int) (y + fieldHeight));
				g2.drawLine((int) (x), (int) (y + cellHeight * i), (int) (x + fieldWidth), (int) (y + cellHeight * i));
			}

			for (int i = 0; i < 8; i++) {
				for (int j = 0; j < 8; j++) {
					if (Board.board[i][j] != null) {
						int unitX = (int) (x + cellWidth * i + 1);
						int unitY = (int) (y + cellHeight * j + 1);
						Image imageUnit = Board.board[i][j].getUnitImage();
						
						if (Board.selectedUnit != null) {
							if (i == (int) Board.selectedUnit.getX() && j == (int) Board.selectedUnit.getY()) {
								g2.setColor(Color.yellow);
								g2.setStroke(new BasicStroke(4));
								g2.draw(new Rectangle2D.Double(unitX, unitY, cellWidth - 4, cellHeight - 4));
							}
						}

						g2.drawImage(imageUnit, unitX, unitY, (int) cellWidth - 2, (int) cellHeight - 2, this);
					}
				}
			}
		}
	}
}
