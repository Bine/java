package abilities;

import static unit.Classes.VAMPIRE;

import exeption.InvalidParamException;
import exeption.UnitIsDeadException;
import unit.Unit;

public class ArcherAttack extends Ability {
	private Unit unit;
	
	public ArcherAttack(Unit unit, int damage) {
		super(damage, 5);
		this.unit = unit;
	}

	public void action(Unit target) throws UnitIsDeadException, InvalidParamException {
		target.takeDamage(damage);
		
//		int distance = this.unit.getLocation().distance(target.getLocation());
//		if ( distance == 1 ) {
//			target.isAlive();
//			
//			unit.takeDamage(target.getAbility().getDamage() / 2);
//		
//			if (target.getUnit_class() == VAMPIRE) {
//				target.getState().hp += target.getAbility().getDamage() / 6;
//			
//				if (target.getState().hp > target.getState().maxhp) {
//					target.getState().hp = target.getState().maxhp;
//				}
//			}
//		}
	}
}
