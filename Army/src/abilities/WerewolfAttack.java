package abilities;

import exeption.CanNotInfectException;
import static unit.Classes.*;
import unit.Unit;

public class WerewolfAttack extends Ability {
	private Unit unit;

	public WerewolfAttack(Unit unit, int damage) {
		super(damage, 1);
		this.unit = unit;
	}

	@Override
	public void action(Unit target) throws Exception {
		target.takeDamage(damage);
		
		target.isAlive();

		if (Math.random() < 0.5) {
			if ( target.getUnit_class() == VAMPIRE ) {
				throw new CanNotInfectException();
			}
			infect(target);
		}
		
		unit.takeDamage(target.getAbility().getDamage() / 2);
	
		if (target.getUnit_class() == VAMPIRE) {
			target.getState().hp += target.getAbility().getDamage() / 6;
		
			if (target.getState().hp > target.getState().maxhp) {
				target.getState().hp = target.getState().maxhp;
			}
		}
	}
	
	private void infect(Unit victim) {
		victim.getState().isWerewolf = true;
	}

}
